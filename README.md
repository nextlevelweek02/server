## Inicializando o projeto

yarn init -y

yarn add typescript

yarn tsc --init (arquivo de configuração do typescript: tsconfig.json)
Obs: no arquivo tsconfig.json foi modificado o target para es2017

yarn add ts-node-dev -D (executa o servidor e verifica mudanças no código)

No package.json acrescentar o script de start "tsnd 'arquivo'" para inicializar o server, também serão adicionadas algumas flags:

--transpile-only : não verifica erros na conversão do typescript para o js, acelerando o processo de execução da aplicação.

--ignore-watch 'path': ignorar a verificação nos arquivos.

--respawn: toda vez que houver mudança o código reinicia sozinho

"scripts": {
  "start": "tsnd --transpile-only --ignore-watch node-modules --respawn src/server.ts"
},

yarn add express (framework com algumas funcionalidades para facilitar o backend)

Agora no seu arquivo de inicialização, use essa padrão para inicializá-lo:
import express from 'express'
const app = express()
app.use(express.json())
app.listen(3333)

## Criando o Banco de Dados

yarn add knex sqlite3

criar um knexfile.ts para configurar o funcionamento do knex (pois por padrão ele não entende typescript)

criar as migrations (referencia: http://knexjs.org/#Installation-migrations)
Obs: por ser typescript, a mesma deve ser criado do zero e o nome do arquivo deve conter algum número ou data (YYYYMMDD) para deixar os arquivos na ordem correta.

para facilitar, crie mais um script no package.json, da seguinte forma:
"knex:migrate": "knex --knexfile knexfile.ts migrate:latest"

