import Knex from 'knex'

const tableName = 'classes'

export async function up(knex: Knex) {
  return knex.schema.createTable(tableName, table => {
    table.increments('id').primary();
    table.string('subject').notNullable();
    table.decimal('cost').notNullable();

    table.integer('user_id')
      .notNullable()
      .references('id')
      .inTable('users')
      // siginifica que quando um usuário for atualizado ou deletado, essa mesma ação
      // irá ocorrer em cascata em todos as aulas dadas por aquele professor.
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
  })
}

export async function down(knex: Knex) {
  return knex.schema.dropTable(tableName)
}