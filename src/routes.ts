import express, { response } from 'express'

const routes = express.Router()

import ClassesController from './controllers/ClassesController'
const classesController = new ClassesController()
routes.get('/classes', classesController.index)
routes.post('/classes', classesController.create)

import ConnectionsController from './controllers/ConnectionsController'
const connectionsController = new ConnectionsController()
routes.get('/connections', connectionsController.index)
routes.post('/connections', connectionsController.create)

export default routes