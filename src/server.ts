import express from 'express'
import cors from 'cors'
import routes from './routes'

const app = express()

app.use(cors()) //para a aplicação poder ser acessada à partir de qualquer URL
app.use(express.json()) //converte as requisições para formato JSON
app.use(routes)

app.listen(3333)